import Vue from 'vue'
import VueRouter from 'vue-router'

import Peliculas from '../views/Peliculas'
import Actores from '../views/Actores'

import Pelicula from '../views/Pelicula'
import Actor from '../views/Actor'

Vue.use(VueRouter)

  const routes = [
  {
    path: '/',
    name: 'Peliculas',
    component: Peliculas
  },
  {
    path: '/pelicula/:id',
    name: 'Pelicula',
    component: Pelicula
  },
  {
    path: '/actor',
    name: 'Actores',
    component: Actores
  },
  {
    path: '/actor/:id',
    name: 'Actor',
    component: Actor
  },

]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
