class PeliculaServices {
    axios
    baseUrl

    constructor(axios,baseUrl) {
        this.axios = axios;
        this.baseUrl = baseUrl;
        this.patch = "films";
    }

    getAll(){
        let self = this;
        return self.axios.get(`${self.baseUrl}/${self.patch}`);
    }
    save(data){
        let self = this;
        return self.axios.post(`${self.baseUrl}/${self.patch}/`,data);
    }
    get(id) {
        let self = this;
        return self.axios.get(`${self.baseUrl}/${self.patch}/${id}/`);
    }
    update(id,data){
        let self = this;
        return self.axios.put(`${self.baseUrl}/${self.patch}/${id}`,data);
    }
    delete(id){
        let self = this;
        return self.axios.delete(`${self.baseUrl}/${self.patch}/${id}`);
    }
}

export default PeliculaServices