import Vue from 'vue'
import Vuex from 'vuex'

import services from './services'

import ActorStore from './ActorStore'
import PeliculaStore from './PeliculaStore'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    services
  },
  mutations: {
  },
  actions: {
  },
  modules: {
    ActorStore,
    PeliculaStore
  }
})
