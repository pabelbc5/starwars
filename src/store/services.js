import Axios from 'axios'
import ActorServices from '../services/ActorServices'
import PeliculaServices from '../services/PeliculaServices'

Axios.defaults.headers.common.Accept = 'application/json';
let  URL_base = 'https://swapi.dev/api';
export default {
    actorServices: new ActorServices(Axios,URL_base),
    peliculaServices: new PeliculaServices(Axios,URL_base),
}   