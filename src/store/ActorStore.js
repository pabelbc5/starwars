//import axios from 'axios';
export default {
    state: {
        actores:[],
        actor:{},
    },
    mutations: {
        
    },
    actions: {
        getActores({state,rootState}){
            rootState.services.actorServices.getAll()
                .then(function (response) {
                    state.actores = response.data.results;
                })
                .catch(function (error) {
                    console.log(error);
                });
        },
        getActor({state,rootState},id){
            rootState.services.actorServices.get(id)
                .then(function (response) {
                    console.log(response.data);
                    state.actor = response.data;
                })
                .catch(function (error) {
                    console.log(error);
                });
        }
    },
    getters: {}
}