//import axios from 'axios';
export default {
    state: {
        peliculas:[],
        pelicula:{},
    },
    mutations: {
        
    },
    actions: {
        getPeliculas({state,rootState}){
            rootState.services.peliculaServices.getAll()
                .then(function (response) {
                    state.peliculas = response.data.results;
                })
                .catch(function (error) {
                    console.log(error);
                });
        },
        getPelicula({state,rootState},id){
            rootState.services.peliculaServices.get(id)
                .then(function (response) {
                    state.pelicula = response.data;
                })
                .catch(function (error) {
                    console.log(error);
                });
        }
        /* getPelicula(state,url){
            axios.get(url)
                .then(function(res){
                    console.log(res);
                })
        } */
    },
    getters: {}
}